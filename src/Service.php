<?php
namespace Sportily;

use Cache;
use Config;

class Service {

    private $name;

    private $class;

    public function __construct($name) {
        $this->name = $name;
        $this->class = 'Sportily\\' . $name;
    }

    public function all($query = []) {
        return $this->cachedDelegate('all', [ $query ]);
    }

    public function retrieve($id, $query = []) {
        return $this->cachedDelegate('retrieve', [ $id, $query ]);
    }

    public function create($data) {
        return $this->delegate('create', [ $data ]);
    }

    public function update($id, $data) {
        return $this->delegate('update', [ $id, $data ]);
    }

    public function delete($id) {
        return $this->delegate('delete', [ $id ]);
    }

    private function delegate($method, $arguments) {
        return forward_static_call_array([ $this->class, $method ], $arguments);
    }

    private function cachedDelegate($method, $arguments) {
        $result = null;
        $cache_key = strtolower($this->name . '/' . http_build_query($arguments));
        return $this->delegate($method, $arguments);
    }

}
