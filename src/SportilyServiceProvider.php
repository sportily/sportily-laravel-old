<?php
namespace Sportily;

use Illuminate\Support\ServiceProvider;

class SportilyServiceProvider extends ServiceProvider {

    private static $resources = [
        'age_group' => 'AgeGroup',
        'calendar_event' => 'CalendarEvent',
        'club' => 'Club',
        'contact' => 'Contact',
        'competition' => 'Competition',
        'document' => 'Document',
        'division' => 'Division',
        'division_entry' => 'DivisionEntry',
        'fixture' => 'Fixture',
        'fixture_event' => 'FixtureEvent',
        'gallery' => 'Gallery',
        'member' => 'Member',
        'organisation' => 'Organisation',
        'page' => 'Page',
        'participant' => 'Participant',
        'person' => 'Person',
        'post' => 'Post',
        'registration' => 'Registration',
        'role' => 'Role',
        'season' => 'Season',
        'link' => 'Link',
        'team' => 'Team',
        'user' => 'User',
        'venue' => 'Venue',
        'video' => 'Video',
        'website' => 'Website'

    ];

    public function register() {
        foreach (self::$resources as $name => $class) {
            $this->app->bind('sportily.' . $name, function() use($class) {
                return new Service($class);
            });
        }
    }

}
