<?php
namespace Sportily\Facades;

use Illuminate\Support\Facades\Facade;

class Post extends Facade {

    protected static function getFacadeAccessor() {
        return 'sportily.post';
    }
}
