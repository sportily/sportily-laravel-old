<?php
namespace Sportily\Facades;

use Illuminate\Support\Facades\Facade;

class Document extends Facade {

    protected static function getFacadeAccessor() {
        return 'sportily.document';
    }
}
