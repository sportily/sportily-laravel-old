<?php
namespace Sportily\Facades;

use Illuminate\Support\Facades\Facade;

class FixtureEvent extends Facade {

    protected static function getFacadeAccessor() {
        return 'sportily.fixture_event';
    }
}
