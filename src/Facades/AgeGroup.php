<?php
namespace Sportily\Facades;

use Illuminate\Support\Facades\Facade;

class AgeGroup extends Facade {

    protected static function getFacadeAccessor() {
        return 'sportily.age_group';
    }
}
