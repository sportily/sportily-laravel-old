<?php
namespace Sportily\Facades;

use Illuminate\Support\Facades\Facade;

class Division extends Facade {

    protected static function getFacadeAccessor() {
        return 'sportily.division';
    }
}
