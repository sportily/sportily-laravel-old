<?php
namespace Sportily\Facades;

use Illuminate\Support\Facades\Facade;

class Team extends Facade {

    protected static function getFacadeAccessor() {
        return 'sportily.team';
    }
}
