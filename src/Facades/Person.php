<?php
namespace Sportily\Facades;

use Illuminate\Support\Facades\Facade;

class Person extends Facade {

    protected static function getFacadeAccessor() {
        return 'sportily.person';
    }
}
