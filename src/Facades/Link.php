<?php
namespace Sportily\Facades;

use Illuminate\Support\Facades\Facade;

class Link extends Facade {

    protected static function getFacadeAccessor() {
        return 'sportily.link';
    }
}
