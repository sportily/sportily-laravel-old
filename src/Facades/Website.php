<?php
namespace Sportily\Facades;

use Illuminate\Support\Facades\Facade;

class Website extends Facade {

    protected static function getFacadeAccessor() {
        return 'sportily.website';
    }
}
