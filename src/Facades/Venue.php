<?php
namespace Sportily\Facades;

use Illuminate\Support\Facades\Facade;

class Venue extends Facade {

    protected static function getFacadeAccessor() {
        return 'sportily.venue';
    }
}
