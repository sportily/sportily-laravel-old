<?php
namespace Sportily\Facades;

use Illuminate\Support\Facades\Facade;

class Video extends Facade {

    protected static function getFacadeAccessor() {
        return 'sportily.video';
    }
}
