<?php
namespace Sportily\Facades;

use Illuminate\Support\Facades\Facade;

class Organisation extends Facade {

    protected static function getFacadeAccessor() {
        return 'sportily.organisation';
    }
}
