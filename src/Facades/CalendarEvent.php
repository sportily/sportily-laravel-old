<?php
namespace Sportily\Facades;

use Illuminate\Support\Facades\Facade;

class CalendarEvent extends Facade {

    protected static function getFacadeAccessor() {
        return 'sportily.calendar_event';
    }
}
