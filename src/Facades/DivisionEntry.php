<?php
namespace Sportily\Facades;

use Illuminate\Support\Facades\Facade;

class DivisionEntry extends Facade {

    protected static function getFacadeAccessor() {
        return 'sportily.division_entry';
    }
}
