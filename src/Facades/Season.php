<?php
namespace Sportily\Facades;

use Illuminate\Support\Facades\Facade;

class Season extends Facade {

    protected static function getFacadeAccessor() {
        return 'sportily.season';
    }
}
