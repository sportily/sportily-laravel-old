<?php
namespace Sportily\Facades;

use Illuminate\Support\Facades\Facade;

class Participant extends Facade {

    protected static function getFacadeAccessor() {
        return 'sportily.participant';
    }
}
