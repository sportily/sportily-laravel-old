<?php
namespace Sportily\Facades;

use Illuminate\Support\Facades\Facade;

class Contact extends Facade {

    protected static function getFacadeAccessor() {
        return 'sportily.contact';
    }
}
