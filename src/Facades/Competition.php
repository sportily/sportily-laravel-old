<?php
namespace Sportily\Facades;

use Illuminate\Support\Facades\Facade;

class Competition extends Facade {

    protected static function getFacadeAccessor() {
        return 'sportily.competition';
    }
}
