<?php
namespace Sportily\Facades;

use Illuminate\Support\Facades\Facade;

class Club extends Facade {

    protected static function getFacadeAccessor() {
        return 'sportily.club';
    }
}
